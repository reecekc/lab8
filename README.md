Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?
    Console logging is only printing logs at the INFO level by setting the ConsoleHandler level to INFO while the file logging is taking care of logging across all levels by setting the FileHandler to ALL.
1.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
     This comes from the JUnit5 Library response to having a test disabled. This is written to the logger when a test is disabled.
1.  What does Assertions.assertThrows do?
    This asserts that exception of the supplied executable throws an exception of a specified type. If no exception is thrown or if an exception of a different type is thrown then the method will fail.
1.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)
        Serialization runtime associates with each serializable class a version number which is used during deserialization to verify that sender and receiver of a serialized object have loaded classes for that object that are compatible with respect to serialization. If the receiver and sender have different serialVersionUID then deserialization will result in an InvalidClassException.
    2.  Why do we need to override constructors?
        We need to override the constructors in order to pass data into the super class. We need constructors to create a TimerException class with Exception as its parent.
    3.  Why we did not override other Exception methods?
        We do not need to change the functionality of the methods already present in the Exception class.
1.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)
    A static block holds a block of code that is executed at the time of loading a class. This block is executed before runtime allowing for only 1 instance to be created across all classes. This specific block is used for setup of the logger properties from the logger.properties file.
1.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
    The file format that README.md uses is the Markdown format for formatting text. bitbucket uses the Markdown format in their README.md's and pull request descriptions and comments to format text.
1.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)
    The test was failing because of the position of the TimerException throw in the try catch block. This try catch block was not designed to handle catching a TimerException and trying to perform arithmetic on a NULL object. The way to change this problem is to remove the portion that throws the TimerException from the try catch block and allow it to throw back to the test. 
1.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)
    The problem is that we are never handling the TimerException in the try catch block. We need to handle this exception and set the object to a non-NULL value thus allowing us to perform the arithmetic safely.
1.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel)
1.  Make a printScreen of your eclipse Maven test run, with console
1.  What category of Exceptions is TimerException and what is NullPointerException
    TimerException is an exception that is checked at compile time, NullPointerException is a RuntimeException that is thrown when the program attempts to use an object reference that has a NULL value.
1.  Push the updated/fixed source code to your own repository.
